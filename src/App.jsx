import React, { useEffect, useState } from 'react';
import Header from './components/Header';
import Body from './components/Body';
import Footer from './components/Footer';
import { connect } from 'react-redux';
import { fetchFilms } from './actions';
import { fetchCharacters } from './actions';

import './App.scss';

const App = props => {
	const [message, setMessage] = useState('Loading...');
	const { films, fetchFilms, characters, fetchCharacters } = props;

	useEffect(() => {
		// TODO I've never disabling lint rules :)
		// eslint-disable-next-line react-hooks/exhaustive-deps
		fetchFilms();
		fetchCharacters();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	useEffect(() => {
		if (/Error/.test(films[0]) || /Error/.test(characters[0])) {
			setMessage('Some problem with fetching data. Please check the console of your browser!');
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	return (
		<>
			{films.length && typeof films[0] !== 'string' ? (
				<>
					<Header />
					<Body />
					<Footer />
				</>
			) : (
				<h1>{message}</h1>
			)}
		</>
	);
};

const mapStateToProps = ({ films, characters }) => {
	return { films, characters };
};

export default connect(mapStateToProps, { fetchFilms, fetchCharacters })(App);
