export default {
	fetchFilms: 'FETCH_FILMS',
	fetchCharacters: 'FETCH_CHARACTERS',
	selectFilm: 'SELECTED_FILM',
	selectCharacter: 'SELECTED_CHARACTER',
	sortFilms: 'SORT_FILMS',
};
