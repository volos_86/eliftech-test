import actionTypes from '../actionTypes';
import { request } from 'graphql-request';

const queryFilms = `{
  movies {
    items {
      title
      release
      rating
      trivia {
        universeTimeline
      }
      media {
        type
        src
      }
      cast {
        id
        name
        castName
        media {
          type
          src
        }
      }
    }
  }
}`;

const queryCharacters = `{
  characters {
    items {
      id
      name,
      castName
      media {
        type
        src
      }
    }
  }
}`;

export const selectFilm = film => {
	return {
		type: actionTypes.selectFilm,
		payload: film,
	};
};

export const sortFilms = sortingType => {
	return {
		type: actionTypes.sortFilms,
		payload: sortingType,
	};
};

export const selectCharacter = film => {
	return {
		type: actionTypes.selectCharacter,
		payload: film,
	};
};

// TODO should be fixed according DRY principles
export const fetchFilms = () => async dispatch => {
	let res;
	try {
		res = await request('http://starwars.asteria.ai/graphql', queryFilms);
	} catch (e) {
		console.error('Something wrong: ', e);
		res = [`Error: ${e}`];
	}

	dispatch({
		type: actionTypes.fetchFilms,
		payload: res?.movies?.items ? res.movies.items : res,
	});
};

export const fetchCharacters = () => async dispatch => {
	let res;
	try {
		res = await request('http://starwars.asteria.ai/graphql', queryCharacters);
	} catch (e) {
		console.error('Something wrong: ', e);
		res = [`Error: ${e}`];
	}

	dispatch({
		type: actionTypes.fetchCharacters,
		payload: res?.characters?.items ? res.characters.items : res,
	});
};
