import React from 'react';
import { Grid, Image, Button } from 'semantic-ui-react';

const CastComponent = ({ cast }) => {
	const addButton = (
		<Grid.Column textAlign={'center'} className={'sw-bigmodal-title'} verticalAlign={'middle'}>
			<div onClick={e => console.log('Add clicked', e)}>
				<Button circular icon="plus" size="massive" className={'sw-addbutton'} />
				<h3>ADD</h3>
			</div>
		</Grid.Column>
	);

	const castElements = cast.map(item => {
		const { name, id } = item;
		const img = item.media.filter(el => el.type === 'image');
		return (
			<Grid.Column key={id} textAlign={'center'} className={'sw-bigmodal-title'}>
				<Image src={img[0].src} size="small" circular centered className={'sw-image'} />
				<h3>{name.toUpperCase()}</h3>
			</Grid.Column>
		);
	});

	return (
		<Grid centered className={'sw-bigmodal-cast'}>
			<Grid.Row className={'sw-bigmodal-title'}>
				<h2 className={'sw-title--black'}>CAST</h2>
			</Grid.Row>
			<Grid.Row>
				<Grid.Column>
					<Grid relaxed columns={5}>
						{castElements}
						{addButton}
					</Grid>
				</Grid.Column>
			</Grid.Row>
		</Grid>
	);
};
export default CastComponent;
