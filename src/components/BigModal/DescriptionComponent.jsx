import React from 'react';
import { Grid } from 'semantic-ui-react';

const DescriptionComponent = ({ title, description, rating, release }) => {
	const duration = '2h10m'; // TODO add magick number, qql doesn't have duration
	const year = new Date(release).getFullYear();
	return (
		<Grid style={{ backgroundColor: '#fff' }}>
			<Grid.Row centered className={'sw-bigmodal-title'}>
				<h1 className={'sw-bigmodal-title--green'}>{title.toUpperCase()}</h1>
			</Grid.Row>
			<Grid.Row>
				<Grid.Column textAlign={'center'} className={'sw-bigmodal-title'}>
					<h2>
						<span>{year}</span>
						<span>{rating}</span>
						<span>{duration}</span>
					</h2>
				</Grid.Column>
			</Grid.Row>
			<Grid.Row className={'sw-bigmodal-description'}>
				<p>{description}</p>
			</Grid.Row>
		</Grid>
	);
};
export default DescriptionComponent;
