import React from 'react';
import { Grid, Embed, Icon } from 'semantic-ui-react';
import { loremIpsum } from 'lorem-ipsum';
import DescriptionComponent from './DescriptionComponent';
import CastComponent from './CastComponent';
import { connect } from 'react-redux';
import { selectFilm } from '../../actions';

import './index.scss';

const BigModal = ({ selectedFilm, selectFilm }) => {
	// TODO qql doesn't provide any other video link, hardcoded but it can be easily fixed
	const videoId = 'https://www.youtube.com/watch?v=bD7bpG-zDJQ'.split('v=')[1];
	if (!selectedFilm) {
		return null;
	}
	const { title, rating, release, cast } = selectedFilm;
	const description = loremIpsum({ count: 5, units: 'sentence' });
	const close = e => {
		selectFilm(null);
	};

	return (
		<div>
			<Grid centered className={'sw-bigmodal'}>
				<Grid.Row style={{ paddingBottom: 0 }}>
					<Grid.Column width={10} className={'sw-youtube--white sw-youtube'}>
						<Icon
							name="close"
							size={'huge'}
							color={'black'}
							className={'sw-bigmodal-icon'}
							onClick={e => close(e)}
						/>
						<Embed id={videoId} source="youtube" />
					</Grid.Column>
				</Grid.Row>
				<Grid.Row>
					<Grid.Column width={10}>
						<DescriptionComponent
							title={title}
							rating={rating}
							description={description}
							release={release}
						/>
					</Grid.Column>
				</Grid.Row>
				<Grid.Row>
					<Grid.Column width={10}>
						<CastComponent cast={cast} />
					</Grid.Column>
				</Grid.Row>
			</Grid>
		</div>
	);
};

const mapStateToProps = ({ selectedFilm }) => {
	return { selectedFilm };
};

export default connect(mapStateToProps, { selectFilm })(BigModal);
