import React from 'react';
import { connect } from 'react-redux';
import HorizontalScroller from '../ScrollComponent';
import CardComponent from '../Card';

import './index.scss';

const Body = ({ films, sorted, selectedCharacters }) => {
	const sortByRelease = (first, second) => {
		return new Date(first.release) - new Date(second.release);
	};

	const sortBySWTimeline = (first, second) => {
		let reg = /^B/;
		let firstTime = first.trivia.universeTimeline.split(' ');
		let secondTime = second.trivia.universeTimeline.split(' ');
		if (firstTime[1] !== secondTime[1]) {
			return reg.test(firstTime[1]) ? -1 : 1;
		}
		if (reg.test(firstTime[1])) {
			return +firstTime[0] > +secondTime[0] ? -1 : 1;
		}
		return +firstTime[0] > +secondTime[0] ? 1 : -1;
	};

	const sortedFilms = sorted === 'release' ? films.sort(sortByRelease) : films.sort(sortBySWTimeline);

	const renderedTimelines = sortedFilms.map((item, index) => {
		let dimmed;
		if (selectedCharacters) {
			dimmed = !item.cast.some(characters => characters.id === selectedCharacters.id);
		}
		const config = {
			position: index % 2 ? 'top' : 'bottom',
			dimmed,
		};
		return <CardComponent key={item.release} data={item} config={config} />;
	});
	return (
		<>
			<div className={'sw-body'}>
				<HorizontalScroller invert sensibility={200}>
					{renderedTimelines}
				</HorizontalScroller>
			</div>
		</>
	);
};

const mapStateToProps = ({ films, sorted, selectedCharacters }) => {
	return { films, sorted, selectedCharacters };
};

export default connect(mapStateToProps)(Body);
