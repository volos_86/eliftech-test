import React from 'react';
import { Card, Image } from 'semantic-ui-react';
import { selectFilm } from '../../actions';
import { connect } from 'react-redux';

import './index.scss';

/**
 * @param data - array with objects
 * @param type - big or small card
 * @param position - 'top', 'bottom'. 'middle' if undefined
 * @return {*} - element
 * @constructor
 */
const CardComponent = ({ data, config: { type, position, dimmed } = {}, selectFilm }) => {
	const { title } = data;
	const imgUrl = data.media.filter(item => item.type === 'image')[0].src;

	const clickFilm = () => {
		selectFilm(data);
	};
	const bigCard = (
		<Card centered>
			<Image src={imgUrl} wrapped ui={false} />
		</Card>
	);
	const smallCard = (
		<>
			<div
				className={`sw-card ${position || ''} ${dimmed ? 'sw-card--dimmed' : ''}`}
				onClick={dimmed ? null : clickFilm}
			>
				<Image size={'small'} className={'sw-card-image'} src={imgUrl} wrapped />
				<div className={'sw-card-title sw-card-title--green'}>{title}</div>
			</div>
		</>
	);
	return <>{type === 'big' ? bigCard : smallCard}</>;
};
export default connect(null, { selectFilm })(CardComponent);
