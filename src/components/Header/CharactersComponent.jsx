import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { selectCharacter } from '../../actions';
import { Grid, Image } from 'semantic-ui-react';

const CharactersComponent = ({ characters, selectCharacter, selectedCharacters }) => {
	const [clickedAvatar, setClickedAvatar] = useState(null);

	const clickAvatar = id => {
		if (clickedAvatar === id) {
			setClickedAvatar(null);
			selectCharacter(null);
		} else {
			setClickedAvatar(id);
			selectCharacter(characters.find(item => item.id === id));
		}
	};

	const renderCharactersAvatar = characters.map(item => {
		const { id, name, castName } = item;
		const img = item.media.filter(el => el.type === 'image');
		return (
			<Image
				src={img[0].src}
				key={id}
				title={`${name} as ${castName}`}
				size={clickedAvatar === id ? 'tiny' : 'mini'}
				circular
				className={'sw-image sw-characters-image'}
				onClick={() => clickAvatar(id)}
			/>
		);
	});

	useEffect(() => {
		if (selectedCharacters === null) {
			setClickedAvatar(null);
		}
	}, [selectedCharacters]);

	return (
		<Grid relaxed columns={5}>
			<Grid.Row className={'sw-characters'} verticalAlign={'middle'}>
				{renderCharactersAvatar}
			</Grid.Row>
		</Grid>
	);
};

const mapStateToProps = ({ characters, selectedCharacters }) => {
	return { characters, selectedCharacters };
};

export default connect(mapStateToProps, { selectCharacter })(CharactersComponent);
