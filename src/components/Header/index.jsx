import React, { useState } from 'react';
import { Grid, Image } from 'semantic-ui-react';
import { connect } from 'react-redux';
import logo from '../../assets/sw-logo.svg';
import CharactersComponent from './CharactersComponent';
import { sortFilms } from '../../actions';

import './index.scss';

const Header = ({ sortFilms }) => {
	const [sortStatus, setSortStatus] = useState(true);
	const switchMenu = (val, sortType) => {
		setSortStatus(val);
		sortFilms(sortType);
	};

	return (
		<Grid centered className={'sw-header'}>
			<Grid.Column width={6} verticalAlign={'middle'}>
				<div className={'sw-navigation'}>
					<div className={sortStatus ? 'active' : null} onClick={() => switchMenu(true, 'release')}>
						RELEASE DATE
					</div>
					<div className={!sortStatus ? 'active' : null} onClick={() => switchMenu(false, 'swtime')}>
						CHRONOLOGICAL ORDER
					</div>
				</div>
			</Grid.Column>
			<Grid.Column width={3} verticalAlign={'middle'}>
				<Image centered className={'sw-logo'} src={logo} size="small" />
			</Grid.Column>
			<Grid.Column width={6} verticalAlign={'middle'}>
				<CharactersComponent />
			</Grid.Column>
		</Grid>
	);
};
export default connect(null, { sortFilms })(Header);
