/**
 * VOLOS: Based on this https://github.com/angabar/react-horizontal-scroller
 * add one additional feature
 */

import React from 'react';

import './styles.scss';

class HorizontalScrollContainer extends React.Component {
	static defaultProps = {
		sensibility: 100,
		onScroll: () => {},
		invert: false,
	};

	componentDidMount() {
		document.querySelector('.container').addEventListener('wheel', this.handleWheel);
		document.querySelector('.container').addEventListener('click', this.clickBtn);
	}

	componentWillUnmount() {
		document.querySelector('.container').removeEventListener('wheel', this.handleWheel);
		document.querySelector('.container').removeEventListener('click', this.handleWheel);
	}

	clickBtn = event => {
		event.target.scrollIntoView({
			inline: 'center',
		});
	};

	handleWheel = event => {
		if (event.deltaY < 0) {
			if (this.props.invert) {
				document.querySelector('.container').scrollLeft -= this.props.sensibility;
			} else {
				document.querySelector('.container').scrollLeft += this.props.sensibility;
			}
		}

		if (event.deltaY > 0) {
			if (this.props.invert) {
				document.querySelector('.container').scrollLeft += this.props.sensibility;
			} else {
				document.querySelector('.container').scrollLeft -= this.props.sensibility;
			}
		}
	};

	render() {
		return (
			<div className="container" onScroll={() => this.props.onScroll()}>
				{this.props.children}
			</div>
		);
	}
}

export default HorizontalScrollContainer;
