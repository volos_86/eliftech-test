import { combineReducers } from 'redux';
import actionTypes from '../actionTypes';

const filmsReducer = (state = [], action) => {
	switch (action.type) {
		case actionTypes.fetchFilms:
			return [...action.payload];
		default:
			return state;
	}
};

const charactersReducer = (state = [], action) => {
	switch (action.type) {
		case actionTypes.fetchCharacters:
			return [...action.payload];
		default:
			return state;
	}
};

const selectedFilmReducer = (selectedFilm = null, action) => {
	switch (action.type) {
		case actionTypes.selectFilm:
			return action.payload;
		default:
			return null;
	}
};

const selectedCharacterReducer = (selectedFilm = null, action) => {
	switch (action.type) {
		case actionTypes.selectCharacter:
			return action.payload;
		default:
			return null;
	}
};

const sortReducer = (state = 'release', action) => {
	// release or swtime
	switch (action.type) {
		case actionTypes.sortFilms:
			return action.payload;
		default:
			return state;
	}
};

export default combineReducers({
	films: filmsReducer,
	selectedFilm: selectedFilmReducer,
	characters: charactersReducer,
	selectedCharacters: selectedCharacterReducer,
	sorted: sortReducer,
});
